/**
 * Event Observer/Reporter
 *
 * @requires jquery
 */
 /*jshint unused:false */
var uiEventObserver =(function($,document){
	'use strict';

	return {
		/**
		 * Data to be passed through with raised events
		 */
		additionalEventData: {},
		/**
		 * Event namespacing
		 */
		namespace: 'tagged',
		/**
		 *
		 */
		eventQueue: [],
		/**
		 * User defined Callback Handler
		 */
		callback: null,
		//http://www.quirksmode.org/dom/events/index.html
		//http://www.w3.org/TR/DOM-Level-3-Events/#event-types
		eventTypes: {
			'document': [
				'load',
				'unload',
				'DOMLinkAdded',
				'DOMLinkRemoved',
				'DOMMetaAdded',
				'DOMMetaRemoved',
				'DOMWillOpenModalDialog',
				'DOMModalDialogClosed'
			],
			'mutation': [
				'DOMActivate',
				'DOMFocusIn',
				'DOMFocusOut',
				'DOMAttributeNameChanged',
				'DOMAttrModified',
				'DOMCharacterDataModified',
				'DOMContentLoaded',
				'DOMElementNameChanged',
				'DOMNodeInserted',
				'DOMNodeInsertedIntoDocument',
				'DOMNodeRemoved',
				'DOMNodeRemovedFromDocument',
				'DOMSubtreeModified'
			],
			'input': [
				'beforeinput',
				'input'
			],
			'focus': [
				'focus',
				'blur',
				'focusin',
				'focusout'
			],
			'composition': [
				'compositionstart',
				'compositionupdate',
				'compositionend'
			],
			'mouse': [
				'mousedown',
				'mouseenter',
				'mouseleave',
				'mousemove',
				'mouseout',
				'mouseover',
				'mouseup',
				'mousewheel',
				'wheel',
				'click',
				'dblclick',
				'contextmenu'
			],
			'keyboard': [
				'keydown',
				'keypress',
				'keyup',
				'textinput'
			],
			'ui': [
				'resize',
				'scroll'
			],
			'event': [
				'select',
				'load',
				'unload',
				'error',
				'abort'
			],
			'form': [
				'reset',
				'submit'
			],
			'clipboard': [
				'copy',
				'cut',
				'paste'
			]
		},
		/**
		 * Primary Event Responder
		 */
		handleEvent: function(e){
			this.eventQueue.push(e);

			if (typeof(this.callback) === 'function') {
				this.callback.call(this, e);
			}
		},
		/**
		 * Begin watching events
		 */
		startWatching: function(element, eventType) {
			// Empty the event Queue
			this.eventQueue = [];

			if (element === undefined) {
				element = document.body;
			}

			var eventList = this.getFlattenedEventList(eventType);
			this.attachListeners(element, eventList);
		},
		/**
		 *
		 */
		attachListeners: function(element, eventList) {
			var numEvents = eventList.length || 0;
			for ( var i=0; i < numEvents; i++ ) {
				var eventName = eventList[i] + '.' + this.namespace;
				$(element).on(eventName, this.additionalEventData, $.proxy(this.handleEvent, this));
			}
		},
		/**
		 * Stop watching events
		 */
		stopWatching: function(element, eventType, finishCallback) {
			if (element === undefined) {
				element = document.body;
			}

			var eventList = this.getFlattenedEventList(eventType);
			this.removeListeners(element, eventList);
		},
		/**
		 * Remove Event Listeners
		 */
		removeListeners: function(element, eventList) {
			var numEvents = eventList.length || 0;
			for ( var i=0; i < numEvents; i++ ) {
				var eventName = eventList[i] + '.' + this.namespace;
				$(element).off(eventName, $.proxy(this.handleEvent, this));
			}
		},
		/**
		 * Normalize an event type to a one-dimensional array
		 * of event names to bind/unbind.  Examples:
		 * 'mouseover'          => ["mouseover"]
		 * 'keyboard'           => ["keydown", "keypress", "keyup", "textinput"]
		 * ['click', 'dblclick']=> ['click', 'dblclick']
		 * @param mixed eventType
		 * @return Array 
		 */
		getFlattenedEventList: function(eventType) {
			var events = [];
			if ( eventType !== undefined ) {
				// Handle Array type
				if (eventType instanceof Array){
					for (var type in eventType){
						// If array item is a category, include all events
						// in the category
						if (this.eventTypes.hasOwnProperty(type)){
							events = events.concat(this.eventTypes[type]);
						} else {
							// Otherwise, just include the array item
							events.push(eventType[type]);
						}
					}
				} else if (this.eventTypes.hasOwnProperty(eventType)) {
					// If array item is a category, include all events
					events = this.eventTypes[eventType];
				} else {
					// Otherwise, just include the string provided
					events = [eventType];
				}
				return events;
			} else {
				// Flatten all events
				for ( var evtType in this.eventTypes ){
					// safety first!
					if ( this.eventTypes.hasOwnProperty(evtType) ) {
						events = events.concat(this.eventTypes[evtType]);
					}
				}
				return events;
			}
		}
	};

})(window.jQuery, document);